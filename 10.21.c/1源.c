#include <stdio.h>
/// EOF 是文件的结束标志
//end of file
//

//#include <stdio.h>

//int main()
//{
//	int a = 0;
//	int b = 0;
//	//
//	//scanf函数读取失败的时候会返回EOF
//	//如果读取正常的话，返回的是读取到的数据的个数
//	int ret = scanf("%d %d", &a, &b);
//
//	printf("ret = %d\n", ret);
//	printf("a = %d\n", a);
//	printf("b = %d\n", b);
//
//	return 0;
//}

//int main()
//{
//	//建议的作用
//	register int a = 10;
//
//	return 0;
//}

//typedef unsigned int uint;
//
//int main()
//{
//	unsigned int age;
//	uint age2;
//
//	return 0;
//}
//

//static 静态的
//三种用法：
// 1. 修饰局部变量
// 2. 修饰全局变量
// 3. 修饰函数
//

//修饰局部变量
//void test()
//{
//	int a = 0;
//	a++;
//	printf("%d\n", a);
//}
//
//int main()
//{
//	int i = 0;
//
//	while (i<10)
//	{
//		test();
//		i++;
//	}
//
//	return 0;
//}

//修饰全局变量
//声明外部符号
//extern int g_val;
//
//int main()
//{
//	printf("%d\n", g_val);
//
//	return 0;
//}
e/*xtern int a;

int main()
{

	printf("%d", a);
	return 0;
}*/

//声明外部函数
extern int Add(int x, int y);

//修饰函数
int main()
{
	int a = 10;
	int b = 20;
	int c = Add(a, b);
	printf("%d\n", c);

	return 0;
}
extern int add(int a, int b);

i/*nt add(int a, int b);
{
	return a + b;
}

int mian()
{
	int c = 10;
	int d = 10;
	

	
    int e = add(c, d);

	printf("%d", e);

	return 0;
}*/


//#define 定义符号 / 宏
//
//#define M 100
//
//int Max(int x, int y)
//{
//	//if (x > y)
//	//	return x;
//	//else
//	//	return y;
//	return (x > y ? x : y);
//}
//
//#define MAX(x,y)  (x>y?x:y)
//#define ADD(x,y)  ((x)+(y))
//
//int main()
//{
//	//int a = M;
//	//printf("%d\n", M);
//	//printf("%d\n", a);
//
//	int a = 10;
//	int b = 20;
//	//int m = Max(a, b);
//	int m = MAX(a, b);
//	//int m = (a > b ? a : b);
//	printf("%d\n", m);
//
//	return 0;
//}
//
//
//int main()
//{
//	int a = 1;//向内存申请4个字节
//	int * pa = &a;
//	*pa = 20;//解引用操作 - 作用就是通过pa中的地址，找到a, *pa就是a
//	//a = 20;
//
//	printf("%d\n", a);
//
//	/*printf("%p\n", &a);
//	printf("%p\n", pa);*/
//
//
//
//	//pa是一个变量，这个变量是用来存放地址的
//	//而地址又叫指针
//	//所以在C语言中把pa叫指针变量（指针变量是存放指针的变量）
//	// 
//	// 
//	//&a;//& 取地址操作符
//	//printf("%p\n", &a);
//	//&a 取出的是a所占内存的4个字节中第一个字节的地址
//	
//	//printf("%d\n", sizeof(a));
//		
//	return 0;
//}


//int main()
//{
//	char ch = 'w';
//	char* pc = &ch;
//	//通过pc把ch的值改'b'
//	*pc = 'b';
//	printf("%c\n", *pc);//b
//
//	ch = 'q';
//	printf("%c\n", *pc);//q
//
//	return 0;
//}

//
//指针
//1. 指针就是地址
//2. 口头语中指针一般指的是指针变量
//
//
//int main()
//{
//	int a = 10;
//	int* p = &a;
//	return 0;
//}
//

//一个指针变量的大小是多少呢？

//int main()
//{
//	char* pc;
//	int* pi;
//
//	printf("%zd\n", sizeof(pc));
//	printf("%zd\n", sizeof(pi));
//	//x86是32位环境
//	//x64是64位环境
//	return 0;
//}
//static int g_val = 2022;


//
//全局变量本身是具有外部链接属性的
//在A文件中定义的变量，在B文件中可以通过【链接】使用
//但是如果全局变量被static修饰，这个外部链接属性就变成了
//内部链接属性，这个全局变量只能在自己所在的源文件内部使用
//
//static的修饰，会把外部链接属性变成内部链接属性,最终使得全局变量的
//作用域变小了
//

//test.c -- 编译 + 链接  --- .exe


//static int Add(int x, int y)
//{
//	return x + y;
//}

//函数本身是具有外部链接属性的
//被static修饰后，外部链接属性就变成了内部链接属性
//使得这个函数只能在自己所在的源文件内部使用，其他源文件无法使用
//限制了作用域

