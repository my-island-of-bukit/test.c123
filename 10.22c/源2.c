//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include <stdio.h>
//struct Stu
//{
//    char name[20];
//    char sex[5];
//    int age;
//};
//
//void Print(struct Stu* ps)
//{
//    //printf("%s %s %d\n", (*ps).name, (*ps).sex, (*ps).age);
//    //->
//    //结构体的指针->成员名
//    printf("%s %s %d\n", ps->name, ps->sex, ps->age);
//}
//
//int main()
//{
//    int num = 0;
//    struct Stu s = {"zhangsan", "男", 20};
//    struct Stu s2 = { "如花", "女", 18 };
//
//    Print(&s);
//    
//    //结构体变量.成员名
//    //printf("%s\n", s2.name);
//    //printf("%s\n", s2.sex);
//    //printf("%d\n", s2.age);
//
//    return 0;