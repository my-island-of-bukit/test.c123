#define _CRT_SECURE_NO_WARNINGS 1

#include "game.h"


void init_p(char board[row][col], int r, int c)
{
	
	int i = 0;
	for (i = 0; i < r; i++)
	{
		int j = 0;
		for (j = 0; j < c; j++)
		{
			board[i][j] = ' ';
		}
	}
}
void print_p(char board[row][col], int r, int c)
{
	int i = 0;
	for (i = 0; i < r; i++)
	{
		int j = 0;
		for (j = 0; j < c; j++)
		{
			printf(" %c ", board[i][j]);
			if (j < c - 1)
			{
				printf("|");
			}
		}
		printf("\n");
	
		if (i < r-1 )
		{
			for (j = 0; j < c; j++)
			{
				printf("___");
				if (j < c - 1)
					printf("|");
			}
			printf("\n");
		}
			
	}
}
void player(char board[row][col])
{
	printf("请玩家输入");
	int i = 0;
	int j = 0;
	scanf("%d %d", &i, &j);
	if (i > 0 && i <= row && j > 0 && j <= col)
	{
		if (board[i - 1][j - 1] == ' ')
		{
			board[i - 1][j - 1] = '*';
		}
		else
		{
			printf("该座位已被占用，请重新输入");
		}
	}
	else
	{
		printf("坐标非法");
	}
}
void computer(char board[row][col])
{
	printf("电脑输入\n");

	int i = rand() % row;
	int j = rand() % col;

	while (board[i][j] == ' ')
	{
		board[i][j] = '#';
		break;
	}
}
static int is_full(char board[row][col], int r, int c)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < c; j++)
		{
			if (board[i][j] == ' ')
				return 0;
		}
	}
	return 1;
}
char win_p(char board[row][col])
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] == board[i][2] && board[i][0] != ' ');
		{
			return board[i][0];
		}

	}
	for (i = 0; i < col; i++)
	{
		if (board[0][i] == board[1][i] == board[2][i] && board[0][i] != ' ')
		{
			return board[0][i];
		}
	}
	if (board[0][0] == board[1][1] == board[2][2] && board[0][0] != ' ')
	{
		return board[0][0];
	}
	if (board[0][2] == board[1][1] == board[0][1] && board[0][2] != ' ')
	{
		return board[1][1];
	}
	if (is_full(board, row, col) == 1)
	{
		return 'Q';
	}
	return 'C';
}
