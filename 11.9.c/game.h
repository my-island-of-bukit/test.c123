#pragma once
#include <stdio.h>
#include <stdlib.h>    
#include <time.h>  

#define col 10
#define row 10



void init_p(char board[row][col], int r, int c);
void print_p(char board[row][col], int r, int c);
void player(char board[row][col]);
void computer(char board);
char win_p(char board[row][col]);
