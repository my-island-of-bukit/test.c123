#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
    int i = 0;
    scanf("%d", &i);

    while (i > 15)
    {
        printf("%d", i / 16);
        printf("%d", i % 16);
        i = i / 16;
    }
    return;
}