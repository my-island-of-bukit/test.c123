#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
int main(int argc, const char* argv[]) {
    char name[10][10]; int num[10], n = 0;
    void put_name(char na[][10], int nu[10]);                   //声明输入函数
    void sort_name(char na[][10], int nu[10]);                 //声明排序函数
    void search_name(char na[][10], int nu[10], int n);        //声明查找函数

    put_name(name, num);                  //调用输入函数
    sort_name(name, num);                //调用排序函数
    printf("输入你要查找的工号：");
    scanf("%d", &n);
    search_name(name, num, n);          //调用查找函数
    return 0;
}

void put_name(char na[][10], int nu[10])
{
    int i;
    for (i = 0; i < 10; i++)
    {
        printf("input No.%d number:", i + 1);
        scanf("%d", &nu[i]);
        printf("input No.%d name:", i + 1);
        scanf("%s", na[i]);
    }
}

void sort_name(char na[][10], int nu[10])
{
    int i, j, t = 0;
    char med[10];
    for (i = 0; i < 9; i++)
        for (j = 0; j < 9 - i; j++)
        {
            if (nu[j] > nu[j + 1])
            {
                t = nu[j]; nu[j] = nu[j + 1]; nu[j + 1] = t;
                strcpy(med, na[j]);
                strcpy(na[j], na[j + 1]);
                strcpy(na[j + 1], med);
            }
        }
    for (i = 0; i < 10; i++)
    {
        printf("%d\t", nu[i]);
        printf("%s\n", na[i]);
    }
}

void search_name(char na[][10], int nu[10], int n)
{
    int low = 0, high = 9, mid = 0;
    while (low <= high)
    {
        mid = (low + high) / 2;
        if (n > nu[mid])
            low = mid + 1;
        else if (n < nu[mid])
            high = mid - 1;
        else if (n == nu[mid])
        {
            printf("%s\n", na[mid]);
            break;
        }
    }
    if (n != nu[mid])
        printf("查无此数");
}